Flask==1.1.1
Flask-Caching==1.8.0
Flask-Table==0.5.0
Flask-WTF==0.14.3
requests==2.23.0
WTForms==2.2.1