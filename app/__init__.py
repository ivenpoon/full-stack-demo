import os

from flask import Flask
from flask_caching import Cache


def create_app():
  app = Flask(__name__)
  app.config['SECRET_KEY'] = os.urandom(32)
  app.config.from_pyfile('static/config/app.cfg', silent=True)
  return app


# Singleton App and Cache Instances
app = create_app()
cache = Cache(app)

# Make app config available for import in other modules
CACHE_DEFAULT_TIMEOUT = app.config['CACHE_DEFAULT_TIMEOUT']
HOTEL_RATE_SEARCH_URL = app.config['HOTEL_RATE_SEARCH_URL']
RETAIL_PROVIDER = app.config['RETAIL_PROVIDER']
RETAIL_PROVIDER_NAME = app.config['RETAIL_PROVIDER_NAME']
SNAPTRAVEL_PROVIDER = app.config['SNAPTRAVEL_PROVIDER']
