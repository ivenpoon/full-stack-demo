from concurrent.futures import ThreadPoolExecutor
from requests import post
from requests.exceptions import HTTPError

from app import app, cache, CACHE_DEFAULT_TIMEOUT, HOTEL_RATE_SEARCH_URL, RETAIL_PROVIDER, SNAPTRAVEL_PROVIDER
from app.hotel_rate_utilities import combine_hotel_rates


def get_consolidated_hotel_rates(city, checkin, checkout):
  """Return hotel rates for hotels available on both SnapTravel and retail provider.

  :param city: City name.
  :param checkin: Check-in date.
  :param checkout: Check-out date.
  :return: A list of hotel rates.
  """
  try:
    with ThreadPoolExecutor() as executor:
      snaptravel_future = executor.submit(get_hotel_rates, city, checkin, checkout, SNAPTRAVEL_PROVIDER)
      retail_future = executor.submit(get_hotel_rates, city, checkin, checkout, RETAIL_PROVIDER)
    snaptravel_hotel_rates = snaptravel_future.result()
    retail_hotel_rates = retail_future.result()
    combined_hotel_rates = combine_hotel_rates(snaptravel_hotel_rates, retail_hotel_rates)
    return combined_hotel_rates
  except HTTPError as e:
    app.logger.error(e)
    return []


@cache.memoize(timeout=CACHE_DEFAULT_TIMEOUT)
def get_hotel_rates(city, checkin, checkout, provider):
  """Return hotel rates for a particular provider either from cache or from hotel rate search API.

  :param city: City name.
  :param checkin: Check-in date.
  :param checkout: Check-out date.
  :param provider: Hotel rate provider.
  :return: A list of hotel rates.
  :raises: HTTPError when post request fails.
  """
  body = {
    'city': city,
    'checkin': checkin,
    'checkout': checkout,
    'provider': provider
  }
  response = post(HOTEL_RATE_SEARCH_URL, data=body)
  response.raise_for_status()
  hotel_rates = response.json().get('hotels', [])
  return hotel_rates
