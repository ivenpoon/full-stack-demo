from flask_table import Col


class ImageCol(Col):
  """Custom Flask-Table Col that formats an img src url into an image"""
  def __init__(self, name, clazz):
    super(ImageCol, self).__init__(name)
    self.clazz = clazz

  def td_format(self, content):
    if not content:
      return ''
    return "<img class=\"{clazz}\" src={content}>".format(clazz=self.clazz, content=content)


class UnorderedListCol(Col):
  """Custom Flask-Table Col that formats a collection of strings into an unordered list"""
  def __init__(self, name, clazz):
    super(UnorderedListCol, self).__init__(name)
    self.clazz = clazz

  def td_format(self, content):
    items = []
    for item in content:
      items.append('<li>{item}</li>'.format(item=item))
    return "<ul class=\"{clazz}\">{items}</ul>".format(clazz=self.clazz, items=''.join(items))
