from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField


class HotelRateSearchForm(FlaskForm):
  """Custom Flask-Form for searching hotel rates"""
  city = StringField('City')
  checkin = StringField('Check-in')
  checkout = StringField('Check-out')
  submit = SubmitField('Search')
