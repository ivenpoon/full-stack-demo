from flask_table import Table, Col

from app import RETAIL_PROVIDER_NAME
from app.columns import ImageCol, UnorderedListCol


class HotelRateTable(Table):
  """Custom Flask-Table Table that displays hotel rates from hotel rate service"""
  id = Col('Hotel ID')
  hotel_name = Col('Hotel Name')
  num_reviews = Col('Number of Reviews')
  address = Col('Address')
  num_stars = Col('Number of Stars')
  amenities = UnorderedListCol('Amenities', clazz='amenities')
  image_url = ImageCol('Image', clazz='hotel')
  snaptravel_price = Col('SnapTravel Price')
  retail_price = Col('{retail_provider_name} Price'.format(retail_provider_name=RETAIL_PROVIDER_NAME))

  def sort_url(self, col_id, reverse=False):
      raise NotImplementedError('sort_url not implemented')
