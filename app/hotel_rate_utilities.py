import copy


def combine_hotel_rates(snaptravel_hotel_rates, retail_hotel_rates):
  """Return hotel rates available on both snaptravel and retail provider.

  :param snaptravel_hotel_rates: A list of hotel rates available on snaptravel.
  :param retail_hotel_rates: A list of hotel rates available on retail provider.
  :return: A list of hotel rates available on both snaptravel and retail provider.
  """
  # find matching hotel ids
  snaptravel_map = {hotel['id']: hotel for hotel in snaptravel_hotel_rates}
  retail_map = {hotel['id']: hotel for hotel in retail_hotel_rates}
  matching_hotel_ids = snaptravel_map.keys() & retail_map.keys()

  # combine matching hotels
  matching_hotels = {}
  for hotel_id in matching_hotel_ids:
    matching_hotels[hotel_id] = copy.copy(snaptravel_map[hotel_id])
    # remove price because we copied snaptravel's price
    del matching_hotels[hotel_id]['price']
    # add both snaptravel and retail provider prices
    matching_hotels[hotel_id]['snaptravel_price'] = snaptravel_map[hotel_id]['price']
    matching_hotels[hotel_id]['retail_price'] = retail_map[hotel_id]['price']
  return list(matching_hotels.values())
