from flask import flash, render_template, request

from app import app
from app.forms import HotelRateSearchForm
from app.hotel_rate_service import get_consolidated_hotel_rates
from app.tables import HotelRateTable


# Routes
@app.route('/', methods=['GET', 'POST'])
def index():
  form = HotelRateSearchForm(request.form)
  table = None
  if form.validate_on_submit():
    city = form.city.data
    checkin = form.checkin.data
    checkout = form.checkout.data

    if not city or not checkin or not checkout:
      flash('Woah! Please fill in all the boxes before you press search.')
    else:
      consolidated_hotel_rates = get_consolidated_hotel_rates(city, checkin, checkout)
      if not consolidated_hotel_rates:
        flash('Hmm... can\'t seem to find a deal for {city} from {checkin} to {checkout}.'
              .format(city=city, checkin=checkin, checkout=checkout))
        flash('Maybe try another city or dates?')
      else:
        # TODO: For future improvement, figure out how to make pagination work with Flask-Table, or
        #       alternatively, we can create our own table template and use Flask-Paginate.
        table = HotelRateTable(consolidated_hotel_rates)
  return render_template('index.html', form=form, table=table)
